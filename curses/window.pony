use "lib:ncurses"

primitive _Window

primitive ERR
    fun apply(): I32 => -1

actor Window
    let _win: Pointer[_Window] iso

    new create() =>
        _win = @initscr[Pointer[_Window] iso^]()
        @nodelay[I32](_win, true)
    
    be print(msg: String) =>
        @wprintw[I32](_win, msg.cstring())
    
    be move_print(msg: String, x: U64, y: U64) =>
        @wprintw[I32](_win, msg.cstring())
    
    be get_char(on_get_char: {(I32)} val) =>
        let input = @wgetch[I32](_win)

        if input == ERR() then
            get_char(on_get_char)
        else
            on_get_char(input)
        end
    
    be dispose() =>
        @delwin[I32](_win)
        @endwin[I32]()