use "curses"

actor Main
    new create(env: Env) =>
        with win = Window do
            win.print("Hello World!")
            win.get_char({(char: I32) => None})
        end